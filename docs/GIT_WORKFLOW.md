# Git Workflow Guide

## Backing Up Changes

When you need to backup your changes before performing potentially risky git operations:

1. Create a backup branch:
```bash
git checkout -b backup/[original-branch-name]-[feature]
# Example: git checkout -b backup/DashReDesign-cdm-fixes
```

2. Push the backup branch to remote (optional):
```bash
git push origin backup/[branch-name]
```

## Common Git Operations and Their Risks

### Low Risk Operations
- `git status`: Shows working tree status
- `git fetch`: Downloads objects and refs from remote
- `git diff`: Shows changes between commits
- `git log`: Shows commit logs
- `git branch`: Lists branches

### Medium Risk Operations
- `git pull`: Fetches and merges changes (may cause merge conflicts)
- `git merge`: Merges changes (may cause conflicts)
- `git commit`: Creates a new commit
- `git reset --soft`: Moves HEAD to specified commit (preserves changes)

### High Risk Operations
- `git push --force`: Forcefully updates remote (can overwrite others' work)
- `git reset --hard`: Resets working tree (loses uncommitted changes)
- `git clean -fd`: Removes untracked files
- `git rebase`: Rewrites commit history

## Handling Diverged Branches

When your local branch has diverged from remote:

1. Backup your changes:
```bash
git checkout -b backup/[branch-name]-[date]
```

2. Fetch remote changes:
```bash
git fetch origin
```

3. View differences:
```bash
git log --oneline --graph --decorate --all
```

4. Choose a merge strategy:
   - Merge remote changes: `git merge origin/[branch-name]`
   - Rebase your changes: `git rebase origin/[branch-name]`
   - Cherry-pick specific commits: `git cherry-pick [commit-hash]`

## Recovery Procedures

If something goes wrong:

1. If merge is in progress:
```bash
git merge --abort
```

2. Restore from backup branch:
```bash
git checkout backup/[branch-name]
```

3. Recover deleted commits:
```bash
git reflog
git checkout -b recovery [commit-hash]
```

## Best Practices

1. Always create backup branches before risky operations
2. Use descriptive branch names: `feature/`, `bugfix/`, `backup/`
3. Commit frequently with clear messages
4. Pull changes regularly to minimize divergence
5. Never force push to shared branches
6. Document significant changes

## Project-Specific Guidelines

For SpaceDAO Global UI:

1. Branch Naming:
   - Feature branches: `feature/[feature-name]`
   - Bug fixes: `bugfix/[bug-description]`
   - Backups: `backup/[original-branch]-[feature]`

2. Commit Message Format:
```
type: brief description

- Detailed point 1
- Detailed point 2
```

Types:
- feat: New feature
- fix: Bug fix
- docs: Documentation
- style: Formatting
- refactor: Code restructuring
- test: Adding tests
- chore: Maintenance
