# SpaceDAO UI Assets

Please place the following assets in this directory:

1. `spacedao-light-logo@2x.png` - SpaceDAO light logo
2. `spinningglobeonablackbackgroundforuseasanaccompanyinginaprojectworldandearthconceptvideo-1@2x.png` - Spinning globe background
3. `collisions-on-leo.svg` - Collisions overlay
4. `instrument-icon-selector.svg` - CDM Requests icon
5. `instrument-icon-selector1.svg` - Analytics icon

These assets are required for the proper display of the SpaceDAO UI.
