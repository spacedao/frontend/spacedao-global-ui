import { mount } from '@vue/test-utils'
import { describe, it, expect, vi } from 'vitest'
import CDMDetailsPanel from '../Panels/CDMDetailsPanel.vue'
import { useEventBus } from '@/components/UI/eventBus'

// Mock event bus
vi.mock('@/components/UI/eventBus', () => ({
  useEventBus: vi.fn(() => ({
    emit: vi.fn(),
    on: vi.fn(),
    off: vi.fn()
  }))
}))

describe('CDMDetailsPanel', () => {
  const mockCDMData = {
    id: 'test-cdm-123',
    lat: 45.123,
    lng: -122.456,
    altitude: 100,
    color: '#ff0000',
    arguments: {
      sender: 'test-sender',
      timestamp: '2025-01-30T12:00:00Z',
      md: 5000, // 5km in meters
      pc: 500, // 50% in tenths of a percent
      alt: 400000 // 400km in meters
    }
  }

  it('should not render when no data is provided', () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: null
      }
    })
    expect(wrapper.find('.fixed').exists()).toBe(false)
  })

  it('should render when data is provided', () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: mockCDMData
      }
    })
    expect(wrapper.find('.fixed').exists()).toBe(true)
  })

  it('should display correct CDM ID', () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: mockCDMData
      }
    })
    expect(wrapper.text()).toContain('test-cdm-123')
  })

  it('should format values correctly', () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: mockCDMData
      }
    })
    // Check min distance formatting
    expect(wrapper.text()).toContain('5km')
    // Check probability formatting
    expect(wrapper.text()).toContain('50%')
    // Check altitude formatting
    expect(wrapper.text()).toContain('400 km')
  })

  it('should emit close event when close button is clicked', async () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: mockCDMData
      }
    })
    const closeButton = wrapper.find('button')
    await closeButton.trigger('click')
    
    const eventBus = useEventBus()
    expect(eventBus.emit).toHaveBeenCalledWith('cdm-hovered', null)
  })

  it('should have proper z-index and positioning classes', () => {
    const wrapper = mount(CDMDetailsPanel, {
      props: {
        selectedDataPoint: mockCDMData
      }
    })
    const panel = wrapper.find('.fixed')
    expect(panel.classes()).toContain('z-[100]')
    expect(panel.classes()).toContain('right-4')
    expect(panel.classes()).toContain('bottom-24')
  })
})
