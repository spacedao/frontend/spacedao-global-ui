import { mount } from '@vue/test-utils'
import { describe, it, expect, vi, beforeEach } from 'vitest'
import GlobeCDMView from '../HomeContent/GlobeCDMView.vue'
import Globe from 'globe.gl'
import { useEventBus } from '@/components/UI/eventBus'

// Mock dependencies
vi.mock('globe.gl', () => ({
  default: vi.fn(() => ({
    polygonsData: vi.fn().mockReturnThis(),
    polygonCapColor: vi.fn().mockReturnThis(),
    polygonSideColor: vi.fn().mockReturnThis(),
    polygonStrokeColor: vi.fn().mockReturnThis(),
    backgroundColor: vi.fn().mockReturnThis(),
    customThreeObject: vi.fn().mockReturnThis(),
    customThreeObjectUpdate: vi.fn().mockReturnThis(),
    onCustomLayerHover: vi.fn().mockReturnThis(),
    ringMaxRadius: vi.fn().mockReturnThis(),
    ringPropagationSpeed: vi.fn().mockReturnThis(),
    ringRepeatPeriod: vi.fn().mockReturnThis(),
    ringColor: vi.fn().mockReturnThis(),
    controls: vi.fn(() => ({
      autoRotate: true,
      autoRotateSpeed: 0.15
    })),
    pointOfView: vi.fn()
  }))
}))

vi.mock('@/components/UI/eventBus', () => ({
  useEventBus: vi.fn(() => ({
    emit: vi.fn(),
    on: vi.fn(),
    off: vi.fn()
  }))
}))

vi.mock('@/services/FetchData', () => ({
  fetchCDMData: vi.fn(),
  fetchDRMData: vi.fn()
}))

describe('GlobeCDMView', () => {
  beforeEach(() => {
    vi.clearAllMocks()
  })

  const mockProps = {
    latestActivities: {
      spatialData: '10',
      collisionData: 7,
      satellitesData: 0
    }
  }

  it('should initialize globe with correct settings', async () => {
    const wrapper = mount(GlobeCDMView, {
      props: mockProps,
      global: {
        provide: {
          globeMode: { value: 'CDM' }
        }
      }
    })

    expect(Globe).toHaveBeenCalled()
    const globeInstance = Globe()
    expect(globeInstance.backgroundColor).toHaveBeenCalledWith('#0a0a0a')
    expect(globeInstance.polygonCapColor).toHaveBeenCalled()
    expect(globeInstance.onCustomLayerHover).toHaveBeenCalled()
  })

  it('should emit cdm-hovered event when sphere is hovered', async () => {
    const wrapper = mount(GlobeCDMView, {
      props: mockProps,
      global: {
        provide: {
          globeMode: { value: 'CDM' }
        }
      }
    })

    const mockCDMData = {
      __data: {
        id: 'test-cdm',
        lat: 45,
        lng: -122,
        altitude: 100,
        color: '#ff0000',
        arguments: {
          sender: 'test',
          timestamp: '2025-01-30'
        }
      }
    }

    // Get the hover handler
    const globeInstance = Globe()
    const hoverHandler = globeInstance.onCustomLayerHover.mock.calls[0][0]
    
    // Call the hover handler
    hoverHandler(mockCDMData)

    // Check if event was emitted with correct data
    expect(wrapper.emitted('cdm-hovered')).toBeTruthy()
    expect(wrapper.emitted('cdm-hovered')[0][0]).toEqual(mockCDMData.__data)
  })

  it('should emit null when hover ends', async () => {
    const wrapper = mount(GlobeCDMView, {
      props: mockProps,
      global: {
        provide: {
          globeMode: { value: 'CDM' }
        }
      }
    })

    const globeInstance = Globe()
    const hoverHandler = globeInstance.onCustomLayerHover.mock.calls[0][0]
    
    // Call the hover handler with null
    hoverHandler(null)

    expect(wrapper.emitted('cdm-hovered')).toBeTruthy()
    expect(wrapper.emitted('cdm-hovered')[0][0]).toBeNull()
  })
})
