<template>
  <div class="fixed inset-0 overflow-hidden" @click="handleBackgroundClick">
    <!-- Fixed position globe container -->
    <div class="w-full h-screen flex items-center">
      <div ref="globeContainer" class="w-full h-full" />
    </div>
    
    <!-- Overlay panels -->
    <div class="fixed inset-0 pointer-events-none">
      <InstrumentPanel position="left" class="!pointer-events-auto" />
    </div>
  </div>
</template>

<script setup lang="ts">
import { ref, onMounted, inject, watch } from 'vue'
import Globe from 'globe.gl'
import * as THREE from 'three'
import * as topojson from 'topojson-client'
import { useEventBus } from '@/components/UI/eventBus'
import InstrumentPanel from '@/components/Panels/InstrumentPanel.vue'
import type { GeoJsonFeature } from '@/types/types'
import type { CDMData, GlobeCDMData } from '@/types/cdm'
import type { DRMData, GlobeDRMData } from '@/types/drm'
import { fetchCDMData, fetchDRMData } from '@/services/FetchData'

const props = defineProps<{
  latestActivities: {
    spatialData: string;
    collisionData: number;
    satellitesData: number;
  }
}>()

const emit = defineEmits<{
  'cdm-selected': [data: CDMData | null],
  'drm-selected': [data: DRMData | null]
}>()

const globeMode = inject('globeMode') as { value: string }
const globeContainer = ref<HTMLElement | null>(null)
const globe = ref<any>(null)
const selectedCDM = ref<CDMData | null>(null)
const selectedDRM = ref<DRMData | null>(null)

// Create sphere geometry once for reuse
const sphereGeometry = new THREE.SphereGeometry(1, 32, 32);  // Smaller base size

// Fetch land data from backend
const fetchLand = async () => {
  const response = await fetch('/static/data/land-110m.json')
  if (!response.ok) {
    throw new Error('Failed to fetch land data')
  }
  return response.json()
}

const loadCDMData = async () => {
  const cdmData = await fetchCDMData();
  // Add ring properties to each CDM data point, using the same color as the sphere
  return cdmData.map(cdm => ({
    ...cdm,
    maxR: 1,            // Smaller maximum radius
    propagationSpeed: 0.5, // Medium propagation speed for balanced ripples
    repeatPeriod: 1000  // Shorter period for more frequent ripples
  }));
}

const loadDRMData = async () => {
  return await fetchDRMData()
}

// Update customThreeObject to handle active state
const customThreeObject = (d: any) => {
  const isActive = (globeMode.value === "DRM" && selectedDRM.value?.id === d.id) || (globeMode.value !== "DRM" && selectedCDM.value?.id === d.id);
  
  console.log('Creating sphere:', {
    id: d.id,
    isActive,
    altitude: d.altitude
  });
  
  // Create base material with basic properties
  const material = new THREE.MeshPhongMaterial({
    color: d.color
  });

  const mesh = new THREE.Mesh(sphereGeometry, material);
  
  // Set base size relative to altitude
  const baseScale = 2.0;  // Base scale for all spheres
  const activeScale = 4.0;  // Larger scale for active sphere
  
  if (isActive) {
    console.log('Applying active scale:', activeScale);
    mesh.scale.set(activeScale, activeScale, activeScale);
  } else {
    mesh.scale.set(baseScale, baseScale, baseScale);
  }

  return mesh;
}

// Handle sphere click with type awareness
const handleSphereClick = (data: any, event: MouseEvent): void => {
  if (event) {
    event.stopPropagation();
  }
  
  if (!data) return;
  
  console.log('Sphere clicked:', data.id);
  
  if (globeMode.value === "DRM") {
    selectedDRM.value = data as DRMData;
    selectedCDM.value = null;
    emit('drm-selected', selectedDRM.value);
  } else {
    selectedCDM.value = data as CDMData;
    selectedDRM.value = null;
    emit('cdm-selected', selectedCDM.value);
  }

  if (globe.value) {
    globe.value.controls().autoRotate = false;
    
    // Adjust point of view with a lower altitude to keep rings visible on surface
    globe.value.pointOfView({
      lat: data.lat + 10, // Reduced offset from 15 to 10
      lng: data.lng,
      altitude: 1.5      // Reduced from 2.0 to 1.5
    }, 1000);
  }
}

// Handle background click
const handleBackgroundClick = (event: MouseEvent): void => {
  // Only handle direct background clicks
  const target = event.target as HTMLElement;
  if (target.closest('.globe-control') || target.closest('.panel')) {
    return;
  }

  selectedCDM.value = null;
  selectedDRM.value = null;
  emit('cdm-selected', null);
  emit('drm-selected', null);

  if (globe.value) {
    globe.value.controls().autoRotate = true;
    globe.value.controls().autoRotateSpeed = 0.15;
  }
}

// Watch for changes in selectedCDM
watch(selectedCDM, (newValue) => {
  console.log('selectedCDM changed:', newValue?.id);
  if (globe.value) {
    const currentData = globe.value.customLayerData();
    // Update both spheres and rings together
    globe.value
      .customLayerData([...currentData])
      .ringsData([...currentData]);
  }
});

// Watch for changes in selectedDRM
watch(selectedDRM, (newValue) => {
  console.log('selectedDRM changed:', newValue?.id);
  if (globe.value) {
    const currentData = globe.value.customLayerData();
    // Update both spheres and rings together
    globe.value
      .customLayerData([...currentData])
      .ringsData([...currentData]);
  }
});

// Clear all globe data and objects
const clearGlobeData = () => {
  if (globe.value) {
    // Clear data layers
    globe.value
      .customLayerData([])
      .ringsData([]);
    
    // Clear selection
    selectedCDM.value = null;
    selectedDRM.value = null;
    emit('cdm-selected', null);
    emit('drm-selected', null);
  }
}

// Initialize the globe with base configuration
const initGlobe = async () => {
  try {
    const land = await fetchLand();
    const feature = topojson.feature(land, land.objects.land) as GeoJsonFeature;

    if (globeContainer.value) {
      // Initialize globe with modern lighting setup
      globe.value = Globe()(globeContainer.value)
        .globeImageUrl('')  // No image, using custom styling
        .showAtmosphere(true)
        .atmosphereColor('#4A0D2C')
        .atmosphereAltitude(0.25)
        .polygonsData(feature.features)
        .polygonCapColor(() => 'rgba(32, 32, 32, 0.6)')
        .polygonSideColor(() => 'rgba(24, 24, 24, 0.6)')
        .polygonStrokeColor(() => 'rgba(48, 48, 48, 0.6)')
        .polygonAltitude(0.01)
        .backgroundColor('#0a0a0a')
        .customThreeObject(customThreeObject)
        .customThreeObjectUpdate((obj, d) => {
          if (obj && globe.value) {
            const coords = globe.value.getCoords(d.lat, d.lng, d.altitude);
            Object.assign(obj.position, coords);
            
            const isActive = (globeMode.value === "DRM" && selectedDRM.value?.id === d.id) || (globeMode.value !== "DRM" && selectedCDM.value?.id === d.id);
            const scale = isActive ? 4.0 : 2.0;
            obj.scale.set(scale, scale, scale);
          }
        })
        .onCustomLayerClick(handleSphereClick);

      // Add ambient and directional light
      const ambientLight = new THREE.AmbientLight(0xffffff, 0.6);
      globe.value.scene().add(ambientLight);

      const directionalLight = new THREE.DirectionalLight(0xffffff, 0.8);
      directionalLight.position.set(1, 1, 1);
      globe.value.scene().add(directionalLight);

      // Set initial view angle
      globe.value.pointOfView({
        lat: 0,
        lng: 0,
        altitude: 2.5
      });

      // Add auto-rotation
      globe.value.controls().autoRotate = true;
      globe.value.controls().autoRotateSpeed = 0.15;
    }
  } catch (error) {
    console.error('Error initializing globe:', error);
  }
};

// Setup globe data
const setupGlobeData = async (gData: GlobeCDMData[] | GlobeDRMData[]) => {
  try {
    if (globe.value) {
      globe.value
        .customLayerData(gData)
        .ringsData(gData)
        .ringColor('color')
        .ringMaxRadius('maxR')
        .ringPropagationSpeed('propagationSpeed')
        .ringRepeatPeriod('repeatPeriod')
        .ringAltitude(0.001); // Keep rings very close to surface
    }
  } catch (error) {
    console.error('Error setting up globe data:', error);
  }
};

// Watch for changes in globeMode and update globe data
watch(globeMode, async (newMode) => {
  console.log('Globe mode changed to:', newMode);
  try {
    // Clear selection
    selectedCDM.value = null;
    selectedDRM.value = null;
    emit('cdm-selected', null);
    emit('drm-selected', null);

    // Reinitialize globe
    await initGlobe();
    
    // Load new data
    if (newMode === "DRM") {
      const drms = await loadDRMData();
      await setupGlobeData(drms);
    } else {
      const cdms = await loadCDMData();
      await setupGlobeData(cdms);
    }
  } catch (error) {
    console.error('Error updating globe mode:', error);
  }
});

// Initialize on mount
onMounted(async () => {
  try {
    await initGlobe();
    
    // Load initial data based on mode
    if (globeMode.value === "DRM") {
      const drms = await loadDRMData();
      await setupGlobeData(drms);
    } else {
      const cdms = await loadCDMData();
      await setupGlobeData(cdms);
    }
  } catch (error) {
    console.error('Error in initial setup:', error);
  }
});
</script>

<style>
:deep(.scene-container) {
  background: transparent !important;
}
</style>
