import { reactive } from 'vue'

const eventBus = reactive({})

export const useEventBus = () => {
  const on = (event, callback) => {
    if (!eventBus[event]) {
      eventBus[event] = []
    }
    eventBus[event].push(callback)
  }

  const off = (event, callback) => {
    if (eventBus[event]) {
      if (callback) {
        // Remove specific callback
        const index = eventBus[event].indexOf(callback)
        if (index > -1) {
          eventBus[event].splice(index, 1)
        }
      } else {
        // Remove all callbacks for this event
        eventBus[event] = []
      }
    }
  }

  const emit = (event, payload) => {
    if (eventBus[event]) {
      eventBus[event].forEach(callback => {
        try {
          callback(payload)
        } catch (error) {
          console.error(`Error in event bus callback for event "${event}":`, error)
        }
      })
    }
  }

  return { on, off, emit }
}