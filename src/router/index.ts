// src/router/index.ts

import { createRouter, createWebHistory } from 'vue-router'
import GenericView from '@/views/GenericView.vue'
import AboutView from '@/views/AboutView.vue'

const routes = [
  { 
    path: '/', 
    component: GenericView,
    name: 'home'
  },
  {
    path: '/about',
    component: AboutView,
    name: 'about'
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
