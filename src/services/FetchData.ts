const fetchCDMData = async () => {
    try {
        
        const response = await fetch('http://localhost:3013/event/CollisionAlert')
        const data = await response.json()

        return data.map((cdm: any) => ({
            id: cdm.tx_hash,
            lat: cdm.args.lat / 1000,
            lng: cdm.args.lng / 1000,
            altitude: (cdm.args.alt / 1000000) / 2,
            color: cdm.args.pc/1000 <= 25 ? 'green' :  cdm.args.pc/1000 <= 75 ? 'orange' : 'red',
            radius: Math.random() / 3 * 10,
            arguments: cdm.args,
        }))
    } catch (error) {
        console.error('Error fetching CDM data:', error)
        return []
    }
}
  

const fetchDRMData = async () => {
    try {
        
        const response = await fetch('http://localhost:3013/event/DisasterReportTC')
        const data = await response.json()
        const storm_colors = ["#29cd00", "#7ab900", "#a6a200", "#ca8600", "#ea5c00", "#ff0000"]
        return data.map((drm: any) => ({
            id: drm.tx_hash,
            lat: drm.args.lat / 1000,
            lng: drm.args.lon / 1000,
            altitude: 0.1,
            color: storm_colors[drm.args.category],
            radius: Math.random() / 3 * 10,
            arguments: drm.args,
        }))
    } catch (error) {
        console.error('Error fetching CDM data:', error)
        return []
    }
}

export {fetchCDMData ,fetchDRMData}