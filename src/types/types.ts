// INTERFACES TS
// ------------ HOME PAGE ------------ //
// Interface 1 - CDMData
export interface CDMData {
  id: string
  lat: number
  lng: number
  alt: string
  size: number
  color: string
  sat1?: string
  sat2?: string
  timeToApproach?: string
  reachingTime?: string
  minimumDistance?: string
  collisionProbability?: number
  isRealData: boolean
}

// Latest activities
export interface LatestActivities {
  spatialData?: string
  collisionData?: number
  satellitesData?: number
}

// GeoJsonFeature
export interface GeoJsonFeature {
  type: string
  features?: any[]
}

// ------------ DASHBOARD PAGE ------------------ //

// Interface 1 - CDMData
export interface CDMRequestData {
  id: number
  nonce: number
  requestor: string
  providers_whitelist: [string, string, string]
  request_time_max: string
  request_time: string
  tca_min: string
  tca_max: string
  rso_list: [string, string]
  state: string
}

// UserData
export interface UserData {
  id: number
  address: string
  name: string
  creationTime: string
  role: string
  belief: number
  active: boolean
}

// AlertMessages
export interface AlertMessages {
  [key: number]: string
}

// CollisionPoint
export interface CollisionPoint {
  name: string
  collisions?: number
  nearMisses?: number
}
// ConfidencePoint
export interface ConfidencePoint {
  name: string
  general?: number
  leo?: number
  meo?: number
  heo?: number
  geo?: number
}

// Satellites
export interface SatelliteDistribution {
  name: string
  value?: number
}

// SpikeData interface
export interface SpikeData {
  id: string
  lat: number
  lng: number
  intensity?: number
  timestamp?: string
}
