export interface DRMArguments {
  timestamp: string;
  source: string;
  predictedTime: string;
  category: number;
  maxWindSpeed: number;
  populationInCat: number;
  populationAtRisk: number;
  countries: number;
}

export interface DRMData {
  id: string;
  lat: number;
  lng: number;
  altitude: number;
  color: string;
  arguments: DRMArguments;
}

export interface GlobeDRMData extends DRMData {
  maxR?: number;
  propagationSpeed?: number;
  repeatPeriod?: number;
}
