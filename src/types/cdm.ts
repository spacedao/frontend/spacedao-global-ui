export interface CDMArguments {
  sender: string;
  timestamp: string;
  md: number;    // Min distance in meters
  pc: number;    // Probability of collision in tenths of percent
  alt: number;   // Altitude in meters
  tca?: string;  // Time of closest approach
  reaching_time?: string;
}

export interface CDMData {
  id: string;
  lat: number;
  lng: number;
  altitude: number;
  color: string;
  arguments: CDMArguments;
}

export interface GlobeCDMData extends CDMData {
  maxR?: number;
  propagationSpeed?: number;
  repeatPeriod?: number;
}
