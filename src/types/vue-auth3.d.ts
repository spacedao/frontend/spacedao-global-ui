// src/vue-auth3.d.ts
declare module 'vue-auth3' {
    import { Plugin } from 'vue';
  
    interface VueAuth3Options {
      loginUrl: string;
      logoutUrl: string;
      fetchUserUrl: string;
      tokenDefaultName?: string;
      tokenType?: string;
      storageType?: 'localStorage' | 'sessionStorage';
      authRedirect?: string;
      notFoundRedirect?: string;
      authEndpoint: string;
      tokenStorage?: 'localStorage' | 'sessionStorage';
      // Add any other options as needed
    }
  
    const VueAuth3: Plugin;
  
    export default VueAuth3;
  }
  