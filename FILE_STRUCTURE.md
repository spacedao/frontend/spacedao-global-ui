# Project File Structure

```
/ (Root Directory)
├── vite.config.ts
├── package.json
├── index.html
├── src
│   ├── main.js
│   ├── App.vue
│   ├── components
│   │   ├── HomeContent
│   │   │   ├── GlobeCDMView.vue
│   │   │   ├── GlobeCDMView1.vue
│   │   │   ├── GlobeCDMView2.vue
│   │   │   ├── GlobeCDMView4.vue
│   │   │   ├── ParentComponent.vue
│   │   │   ├── BubbleOverlay.vue
│   │   │   ├── DatasCDMView.vue
│   │   │   └── InstrumentPanel.vue
│   │   ├── NavBar
│   │   │   └── NavBar.vue
│   │   └── Modals
│   │       ├── LoginForm.vue
│   │       ├── RegistrationForm.vue
│   │       └── Modal.vue
│   ├── layouts
│   │   └── Main.vue
│   └── views
│       └── GenericView.vue
├── public
└── node_modules
```

## Root Directory
- **vite.config.ts**: Configuration file for Vite, including server settings. Change the port number here.
- **package.json**: Lists the project dependencies and scripts.
- **index.html**: The main HTML file for the project.

## src Directory
- **main.js**: Entry point for the application.
- **App.vue**: Root Vue component.

### components Directory
#### HomeContent
- **GlobeCDMView.vue**: Component for visualizing satellite collision data on a globe. Modify spheres and rendering logic here.
- **GlobeCDMView1.vue**, **GlobeCDMView2.vue**, **GlobeCDMView4.vue**: Variants or backups of the globe visualization component.
- **ParentComponent.vue**: Parent component that may contain other components.
- **BubbleOverlay.vue**: Handles overlay bubbles on the globe.
- **DatasCDMView.vue**: Manages data visualization for CDM.
- **InstrumentPanel.vue**: Component for displaying the instrument panel on the interface.

#### NavBar
- **NavBar.vue**: Component for the navigation bar. Modify navigation links and layout here.

#### Modals
- **LoginForm.vue**: Handles user login functionality.
- **RegistrationForm.vue**: Manages user registration.
- **Modal.vue**: Base modal component used for displaying content in a modal dialog.

### layouts
- **Main.vue**: Main layout component, possibly containing the NavBar.

### views
- **GenericView.vue**: A view component that imports and uses `GlobeCDMView.vue`.

## public Directory
- Contains static assets such as images and icons.

## node_modules Directory
- Contains all the npm packages installed for the project.

## Configuration and Scripts
- **tsconfig.json**: TypeScript configuration file.
- **postcss.config.js**: Configuration for PostCSS.

## Development Notes
- **Port Configuration**: Change the port number in `vite.config.ts`.
- **NavBar Modifications**: Likely found in `Main.vue` within the `layouts` directory.
- **Sphere Modifications**: Modify sphere rendering logic in `GlobeCDMView.vue`.
