# Render Flow

## Overview
This document outlines the flow of how the application moves from `App.vue` to `Main.vue` and other components until the page is fully rendered.

1. **Entry Point**
   - **main.js**: The entry point of the application. It initializes the Vue app and mounts it to the DOM.

2. **Root Component**
   - **App.vue**: The root component of the application. It serves as the main container for the app and typically includes the router view.

3. **Routing**
   - **Router View**: The router view within `App.vue` determines which component to render based on the current route.

4. **Layout Component**
   - **Main.vue**: Acts as the primary layout component. It often contains common elements like the NavBar and Footer.

5. **Content Components**
   - **HomeContent Components**: These components, such as `GlobeCDMView.vue`, are rendered within the layout and provide the main functionality of the page.

6. **Rendering**
   - The components are rendered in the order specified by the router and the layout, resulting in the final page view.

## Detailed Flow
```plaintext
main.js
   └── App.vue
       └── <router-view>
           └── Main.vue
               ├── NavBar.vue
               │   ├── Modal.vue
               │   │   ├── LoginForm.vue
               │   │   └── RegistrationForm.vue
               ├── Footer.vue
               └── HomeContent
                   ├── GenericView.vue
                   │   ├── GlobeCDMView.vue
                   │   └── GlobeLegend.vue
                   ├── ParentComponent.vue
                   └── BubbleOverlay.vue
```

## Notes
- The flow may vary based on the specific routing configuration and component structure.
- Ensure that all components are correctly imported and registered within their parent components.
